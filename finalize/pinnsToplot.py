import matplotlib.pyplot as plt
import numpy as np

def cs1(q):
    return 20+10000*q*q

def cs2(q):
    return 15+10000*q*q

def H1(q):
    return 80-50000*q*q

def H2(q):
    return 45-50000*q*q

def trick(H):
    if H>60:
        return 0
    else:
        return np.sqrt((60-H)/60000)

def H2h(H):
    if H>45:
        return 0
    else:
        return np.sqrt((45-H)/50000)

#qs = [0,0.01,0.02,0.03,0.04,0.05]
qs = np.linspace(0,0.05,20)

sziv1 = [H1(i) for i in qs]
sziv2 = [H2(i) for i in qs]
cso1 = [cs1(i) for i in qs]
cso2 = [cs2(i) for i in qs]

h1csi = [H1(i)-cs1(i) for i in qs] 
hs = np.linspace(0,80,100)
h2q = [trick(i)+H2h(i) for i in hs] 

plt.ylim(bottom=0.,top=100)
plt.xlim([0,0.05])
plt.plot(qs,sziv1,label='H1')
plt.plot(qs,sziv2,label='H2')
plt.plot(qs,cso1,label='cso1')
plt.plot(qs,cso2,label='cso2')
plt.plot(qs,h1csi,label='H1-cso1')
plt.plot(h2q,hs,label='H2q')

plt.legend()

plt.show()
#for q in qs:
    #print(H2elsocsovel(q))
