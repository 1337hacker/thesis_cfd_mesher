import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

data = pd.read_csv('u2data0.csv',sep=',')
data = data.drop(data.loc[data['Points:2']!=0].index)

pressure = data[['Points:0','Points:1','p']]
velocity = data[['Points:0','Points:1']]

velocity['Magnitude'] = np.sqrt(data['U:0']**2+data['U:1']**2)
sizingF = pressure.to_numpy()

gx = np.linspace(0,1,21)
gy = np.linspace(0,1,21)
xx,yy = np.meshgrid(gx,gy)
zz = np.zeros(gx.shape)
sortedby0 =sizingF[sizingF[:,1].argsort()]
for idi,i in enumerate(np.linspace(0,1,21)):
    ysort = sortedby0[np.where(sortedby0[:,1]==round(i,2))]
    ysort = ysort[ysort[:,0].argsort()]
    zz=np.vstack((zz,ysort[:,2]))
print(zz)
zz = np.delete(zz,0,0)
with open('u2_p.npy','wb') as f:
    np.save(f,zz, allow_pickle = False)
#plt.scatter(sizingF[:,0],sizingF[:,1])
#plt.show()

from matplotlib.colors import Normalize

vmin = np.min(zz)
vmax = np.max(zz)
# plot a contour
plt.contour(xx, yy, zz, colors='k', linewidths=0.2, levels=50)
plt.contourf(xx, yy, zz, cmap='rainbow', levels=50, norm=Normalize(vmin=vmin, vmax=vmax))
plt.title('Velocity magnitude')
cbar = plt.colorbar(pad=0.03, aspect=25, format='%.0e')
cbar.mappable.set_clim(vmin, vmax)
plt.show()
