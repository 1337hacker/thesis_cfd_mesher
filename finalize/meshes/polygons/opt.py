import numpy as np

def calcTriMid(simp):
    ax=simp[0][0]
    ay=simp[0][1]
    bx=simp[1][0]
    by=simp[1][1]
    cx=simp[2][0]
    cy=simp[2][1]
    d=2*(ax*(by-cy)+bx*(cy-ay)+cx*(ay-by))
    ux=((ax**2+ay**2)*(by-cy)+(bx**2+by**2)*(cy-ay)+(cx**2+cy**2)*(ay-by))/d
    uy=((ax**2+ay**2)*(cx-bx)+(bx**2+by**2)*(ax-cx)+(cx**2+cy**2)*(bx-ax))/d
    return [ux,uy]

def distPoints(a,b):
    return np.sqrt((b[0]-a[0])**2+(b[1]-a[1])**2)

def sizingField(simp,mid,do=False):
    if not do:
        return 1
    ax=simp[0][0]
    ay=simp[0][1]
    bx=simp[1][0]
    by=simp[1][1]
    cx=simp[2][0]
    cy=simp[2][1]
    x = (ax+bx+cx)/3
    y = (ay+by+cy)/3
    mu = np.sqrt((x-mid[0])**2+(y-mid[1])**2)
    if mu <0.5:
        return 0.001**2
    else:
        return mu**2

def calcSurf(simp):
    side1 = distPoints(simp[0],simp[1])
    side2 = distPoints(simp[1],simp[2])
    side3 = distPoints(simp[0],simp[2])

    s = (side1+side2+side3)/2
    T = np.sqrt(s*(s-side1)*(s-side2)*(s-side3))
    return T

def updateVerts(vekt,simps,bc,tips,mid,do=False):
    updated = np.zeros(vekt.shape)
    bc_id, bc_sum, weights = distBC(vekt,bc,tips)
    for idx,p in enumerate(vekt):
        sumT = 0
        if bc_id[idx] != 0:
            #updated[idx] = bc_sum[idx]/bc_id[idx]
            updated[idx] = bc_sum[idx]/weights[idx]
            continue
        for simp in simps:
            if idx in simp:
                T = calcSurf(vekt[simp])
                #print(simp,':','T =',T)
                u = calcTriMid(vekt[simp])
                wsizing = sizingField(vekt[simp],mid,do)

                updated[idx][0] += u[0] * T * wsizing 
                updated[idx][1] += u[1] * T * wsizing 
                sumT += T * wsizing 
        updated[idx][:] = updated[idx][:]/sumT 
    return updated

def distBC(p,bc,tips):
    ids_arr = np.zeros(len(p))
    sum_ = np.zeros((len(p),2))
    weight_sum = np.zeros(len(p))

    for idx,x in enumerate(bc):
        weight = 1
        if x.tolist() in tips.tolist():
            weight = 100000000
        closest = 1000
        id_closest = 0
        for idy,y in enumerate(p) :
            #dist = np.sqrt((y[0]-x[0])**2+(y[1]-x[1])**2)
            dist = distPoints(x,y)
            if dist < closest:
                closest = dist
                id_closest = idy
        ids_arr[id_closest] += 1
        #if same vertex is the closest to multiple bc points, then 1 weighted sum
        sum_[id_closest][0] += x[0] * weight 
        sum_[id_closest][1] += x[1] * weight 
        weight_sum[id_closest] += weight

    return ids_arr, sum_, weight_sum.reshape(ids_arr.shape)
