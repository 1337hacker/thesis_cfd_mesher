import numpy as np
from scipy.spatial import ConvexHull, Delaunay 
from verts import makePoints
import matplotlib.pyplot as plt
from matplotlib import path
from opt import *

def baricenter(points):
    sx = sy = sL = 0
    for i in range(len(points)):   # counts from 0 to len(points)-1
        x0, y0 = points[i - 1]     # in Python points[-1] is last element of points
        x1, y1 = points[i]
        L = ((x1 - x0)**2 + (y1 - y0)**2) ** 0.5
        sx += (x0 + x1)/2 * L
        sy += (y0 + y1)/2 * L
        sL += L
    xc = sx / sL
    yc = sy / sL
    return xc,yc

#p = makePoints(1,20)
#hull = ConvexHull(p)
#polygon = p[hull.vertices]
#domain = path.Path(polygon)

#plist =polygon.reshape(-1).tolist()
#r = np.max(np.abs(plist))
#n = 100
#verts = np.zeros((n,2))

#i = 0
#while i < n:
    #v = makePoints(r,1)
    #if domain.contains_points(v):
        #verts[i] = v 
        #i += 1
#with open('polyVerts.npy','wb') as f:
    #np.save(f,verts,allow_pickle=False)
#with open('p.npy','wb') as f:
    #np.save(f,p,allow_pickle=False)

#--------------------------------------------------------------------#
verts = np.load('polyVerts.npy')
p = np.load('p.npy')
hull = ConvexHull(p)
polygon = p[hull.vertices]
domain = path.Path(polygon)
#---------------------------------------------------------------------#

#plt.plot(verts[:,0],verts[:,1],'gx')
#plt.show()
#QUADRATURE POINTS TEST
k = p[hull.simplices]
weights = np.zeros(len(k))
for idx,x in enumerate(k):
    weights[idx] = distPoints(x[0],x[1])

weights = weights/np.max(weights)
print(np.round(weights,1))
n_bc = 100 
p_ar = np.column_stack((np.linspace(k[0][0][0],k[0][1][0],int(n_bc*weights[0])),np.linspace(k[0][0][1],k[0][1][1],int(n_bc*weights[0]))))
for i in range(1,len(k)):
        p_ar = np.append(p_ar,np.column_stack((np.linspace(k[i][0][0],k[i][1][0],int(n_bc*weights[i])),np.linspace(k[i][0][1],k[i][1][1],int(n_bc*weights[i])))),axis = 0)

#initial positions
for simplex in hull.simplices:
    plt.plot(p[simplex,0],p[simplex,1],'k-')
plt.plot(p_ar[:,0],p_ar[:,1],'ro')
plt.plot(verts[:,0],verts[:,1],'gx')
plt.xlabel("x")
plt.ylabel("y")
plt.savefig("Start.png")
plt.close()
mesh = np.vstack((verts,p))

#Odt TEST
mid = baricenter(polygon)
tri = Delaunay(mesh) 
for i in range(100):
    mesh = updateVerts(mesh,tri.simplices,p_ar,polygon,mid,True)
    tri = Delaunay(mesh)
    if i in [0,1,2,3,4,5,10,20,30,40,49,99,100]:
        plt.figure()
        plt.triplot(mesh[:,0],mesh[:,1],tri.simplices)
        plt.xlabel("x")
        plt.ylabel("y")
        plt.savefig("Iter_sizing{}.png".format(i))
        plt.close()
#plt.show()

