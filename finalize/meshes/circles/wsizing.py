import numpy as np
from scipy.spatial import Delaunay
import matplotlib.pyplot as plt
from verts import * 

def calcTriMid(simp):
    ax=simp[0][0]
    ay=simp[0][1]
    bx=simp[1][0]
    by=simp[1][1]
    cx=simp[2][0]
    cy=simp[2][1]
    d=2*(ax*(by-cy)+bx*(cy-ay)+cx*(ay-by))
    ux=((ax**2+ay**2)*(by-cy)+(bx**2+by**2)*(cy-ay)+(cx**2+cy**2)*(ay-by))/d
    uy=((ax**2+ay**2)*(cx-bx)+(bx**2+by**2)*(ax-cx)+(cx**2+cy**2)*(bx-ax))/d
    return [ux,uy]

def calcSurf(simp):
    ax=simp[0][0]
    ay=simp[0][1]
    bx=simp[1][0]
    by=simp[1][1]
    cx=simp[2][0]
    cy=simp[2][1]
    
    side1 = np.sqrt((bx-ax)**2+(by-ay)**2)
    side2 = np.sqrt((cx-bx)**2+(cy-by)**2)
    side3 = np.sqrt((ax-cx)**2+(ay-cy)**2)

    s = (side1+side2+side3)/2
    T = np.sqrt(s*(s-side1)*(s-side2)*(s-side3))
    return T
def sizingField(simp):
    ax=simp[0][0]
    ay=simp[0][1]
    bx=simp[1][0]
    by=simp[1][1]
    cx=simp[2][0]
    cy=simp[2][1]
    x = (ax+bx+cx)/3
    y = (ay+by+cy)/3
    mu = np.sqrt((x-0)**2+(y-0)**2)
    if mu <0.1:
        return 0.1**2
    else:
        return mu**2

def updateVerts(vekt,simps,bc):
    updated = np.zeros(vekt.shape)
    #avg = np.zeros(vekt.shape)
    for idx,p in enumerate(vekt):
        sumT = 0
        if idx in bc:
            updated[idx][0] = p[0]
            updated[idx][1] = p[1]
            #avg[idx][:] = 1
            continue
        for simp in simps:
            if idx in simp:
                T = calcSurf(vekt[simp])
                print(simp,':','T =',T)
                u = calcTriMid(vekt[simp])
                updated[idx][0] += u[0] * T*sizingField(vekt[simp])
                updated[idx][1] += u[1] * T*sizingField(vekt[simp])
                sumT += T*sizingField(vekt[simp])
       # print(idx,':','sumT =',sumT)
        updated[idx][:] = updated[idx][:]/sumT 
    print(updated) 
    return updated

def makePoints(R,N):
    points = np.zeros((N,2))
    for i in range(N):
        alpha = 2 * np.pi * np.random.rand()
        r = R * np.random.rand() 
        points[i][0] = r * np.cos(alpha) + 0
        points[i][1] = r * np.sin(alpha) + 0
    return points

vekt = np.zeros((12,2))
for i in range(9):
    vekt[i][0] = np.cos(i*2*np.pi/9) 
    vekt[i][1] = np.sin(i*2*np.pi/9)

vekt[9][0] = 0.5
vekt[9][1] = 0
vekt[10][0] = -0.25
vekt[10][1] = 0.25
vekt[11][0] = 0.75
vekt[11][1] = 0

vekt = np.append(vekt, makePoints(0.8,20), axis = 0)
tri = Delaunay(vekt, qhull_options="QJ Pp")
print(tri.simplices)
bcs = np.unique(tri.convex_hull.flatten())

fig, (p1,p2) = plt.subplots(2)
p1.triplot(vekt[:,0], vekt[:,1],tri.simplices)

for i in range(5):
    vekt = updateVerts(vekt,tri.simplices,bcs)
    #print(vekt)
    tri = Delaunay(vekt, qhull_options="QJ Pp")

#print(vekt)
p2.triplot(vekt[:,0], vekt[:,1],tri.simplices)

plt.show()
