import numpy as np
from scipy.spatial import Delaunay
import matplotlib.pyplot as plt
from verts import *
from opt import *

bc = makeQ(1,100)
print(bc)
vekt = np.zeros((12,2))
for i in range(9):
    vekt[i][0] = np.cos(i*2*np.pi/9) 
    vekt[i][1] = np.sin(i*2*np.pi/9)

vekt[9][0] = 0.5
vekt[9][1] = 0
vekt[10][0] = -0.25
vekt[10][1] = 0.25
vekt[11][0] = 0.75
vekt[11][1] = 0

vekt = np.append(vekt, makePoints(0.95,20), axis = 0)
tri = Delaunay(vekt, qhull_options="QJ Pp")
print(tri.simplices)

fig, (p1,p2) = plt.subplots(2)
p1.triplot(vekt[:,0], vekt[:,1],tri.simplices)
p1.set_aspect(1)
p2.set_aspect(1)

for i in range(10):
    vekt = updateVerts(vekt,tri.simplices,bc)
    #print(vekt)
    tri = Delaunay(vekt, qhull_options="QJ Pp")

#print(vekt)
p2.triplot(vekt[:,0], vekt[:,1],tri.simplices)

plt.show()
