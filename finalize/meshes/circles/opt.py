import numpy as np

def calcTriMid(simp):
    ax=simp[0][0]
    ay=simp[0][1]
    bx=simp[1][0]
    by=simp[1][1]
    cx=simp[2][0]
    cy=simp[2][1]
    d=2*(ax*(by-cy)+bx*(cy-ay)+cx*(ay-by))
    ux=((ax**2+ay**2)*(by-cy)+(bx**2+by**2)*(cy-ay)+(cx**2+cy**2)*(ay-by))/d
    uy=((ax**2+ay**2)*(cx-bx)+(bx**2+by**2)*(ax-cx)+(cx**2+cy**2)*(bx-ax))/d
    return [ux,uy]

def calcSurf(simp):
    ax=simp[0][0]
    ay=simp[0][1]
    bx=simp[1][0]
    by=simp[1][1]
    cx=simp[2][0]
    cy=simp[2][1]
    
    side1 = np.sqrt((bx-ax)**2+(by-ay)**2)
    side2 = np.sqrt((cx-bx)**2+(cy-by)**2)
    side3 = np.sqrt((ax-cx)**2+(ay-cy)**2)

    s = (side1+side2+side3)/2
    T = np.sqrt(s*(s-side1)*(s-side2)*(s-side3))
    return T

def updateVerts(vekt,simps,bc):
    updated = np.zeros(vekt.shape)
    bc_id, bc_sum = distBC(vekt,bc)
    for idx,p in enumerate(vekt):
        sumT = 0
        if bc_id[idx] != 0:
            updated[idx] = bc_sum[idx]/bc_id[idx]
            continue
        for simp in simps:
            if idx in simp:
                T = calcSurf(vekt[simp])
                #print(simp,':','T =',T)
                u = calcTriMid(vekt[simp])
                updated[idx][0] += u[0] * T
                updated[idx][1] += u[1] * T
                sumT += T
        updated[idx][:] = updated[idx][:]/sumT 
    return updated

def distBC(p,bc):
    ids_arr = np.zeros(len(p))
    weight_sum = np.zeros((len(p),2))

    for idx,x in enumerate(bc):
        closest = 1000
        id_closest = 0
        for idy,y in enumerate(p) :
            dist = np.sqrt((y[0]-x[0])**2+(y[1]-x[1])**2)
            if dist < closest:
                closest = dist
                id_closest = idy
        ids_arr[id_closest] += 1
        weight_sum[id_closest][0] += x[0] 
        weight_sum[id_closest][1] += x[1] 

    return ids_arr, weight_sum
