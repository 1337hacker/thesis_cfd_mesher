import numpy as np
from scipy.spatial import Delaunay
import matplotlib.pyplot as plt
from verts import *
from opt import *
import time

start_time = time.time()

bc = makeQ(1,100)
vekt = makePoints(0.99,100)

tri = Delaunay(vekt, qhull_options="QJ Pp")

fig, (p1,p2) = plt.subplots(2)
p1.triplot(vekt[:,0], vekt[:,1],tri.simplices)
p1.set_aspect(1)
p2.set_aspect(1)

for i in range(100):
    vekt = updateVerts(vekt,tri.simplices,bc)
    #print(vekt)
    tri = Delaunay(vekt, qhull_options="QJ Pp")

print("elapsed time:", time.time()-start_time)
p2.triplot(vekt[:,0], vekt[:,1],tri.simplices)

plt.show()
