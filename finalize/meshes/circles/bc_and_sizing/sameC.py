import numpy as np
from scipy.spatial import Delaunay
import matplotlib.pyplot as plt
from verts import *
from opt import *
import time

bc = makeQ(1,100)
#vekt = np.load('ezJoasszem.npy')
vekt = makePoints(0.95,200)
tri = Delaunay(vekt, qhull_options="QJ Pp")
print(tri.simplices)

#fig, (p1,p2) = plt.subplots(2)
#p1.triplot(vekt[:,0], vekt[:,1],tri.simplices)
#p1.set_aspect(1)
#p2.set_aspect(1)
tips = np.empty((2,2))

time_start=time.time()

for i in range(100):
    vekt = updateVerts(vekt,tri.simplices,bc,tips,[0,0])
    #print(vekt)
    tri = Delaunay(vekt, qhull_options="QJ Pp")

print("--- %s seconds ---" % (time.time()-time_start))

#print(vekt)
plt.triplot(vekt[:,0], vekt[:,1],tri.simplices)
plt.show()
