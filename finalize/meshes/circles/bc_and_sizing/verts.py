import numpy as np

def distributedVerts(n, shape,sensitivity = 0): 
    width_ratio = shape[1]/shape[0]
    num_y = np.int32(np.sqrt(n/width_ratio)) + 1
    num_x = np.int32(n/num_y)+1

    x = np.linspace(0.,shape[1]-1, num_x, dtype = np.float32)
    y = np.linspace(0.,shape[0]-1, num_y, dtype = np.float32)
    coords = np.stack(np.meshgrid(x,y),-1).reshape(-1,2)

    init_dist = np.min((x[1]-x[0],y[1]-y[0]))

    min_dist = init_dist * (1-sensitivity)
    max_movement = (init_dist - min_dist)/2
    noise = np.random.uniform(low = -max_movement,
                              high = max_movement,
                              size = (len(coords),2))    
    coords += noise
    return coords

def makeQ(R,N):
    points = np.zeros((N,2))
    alphas = np.linspace(0,2*np.pi,N)
    for i,a in enumerate(alphas):
       # alpha = 2 * np.pi * np.random.rand()
        points[i][0] = R * np.cos(a) + 0
        points[i][1] = R * np.sin(a) + 0
    return points

def makePoints(R,N):
    points = np.zeros((N,2))
    for i in range(N):
        alpha = 2 * np.pi * np.random.rand()
        r = R * np.random.rand() 
        points[i][0] = r * np.cos(alpha) + 0
        points[i][1] = r * np.sin(alpha) + 0
    return points

