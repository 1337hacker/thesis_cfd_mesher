import numpy as np
from scipy.spatial import Delaunay
import matplotlib.pyplot as plt
from verts import *
from opt import *

#vekt = np.random.randint(1000,size=(555,2))
#vekt = np.array([[0,8],[1,1],[1,7],[8,1],[0,2],[3,3],[9,8],[2,5],[5,1],[5,7]])
#vekt = makePoints(100,10)
vekt = np.zeros((12,2))
for i in range(9):
    vekt[i][0] = np.cos(i*2*np.pi/9) 
    vekt[i][1] = np.sin(i*2*np.pi/9)

vekt[9][0] = 0.5
vekt[9][1] = 0
vekt[10][0] = -0.25
vekt[10][1] = 0.25
vekt[11][0] = 0.75
vekt[11][1] = 0

vekt = np.append(vekt, makePoints(0.8,20), axis = 0)

tri = Delaunay(vekt, qhull_options="QJ Pp")
 
bcs = np.unique(tri.convex_hull.flatten())

fig, (p1,p2) = plt.subplots(2,figsize=(20,20))
p1.triplot(vekt[:,0], vekt[:,1],tri.simplices)
p1.set_aspect(1)
p2.set_aspect(1)
for i in range(1):
    vekt = updateVerts(vekt,tri.simplices,bcs)
    print(vekt)
    tri = Delaunay(vekt, qhull_options="QJ Pp")

#print(vekt)
p2.triplot(vekt[:,0], vekt[:,1],tri.simplices)

plt.show()
