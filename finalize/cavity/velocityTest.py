from scipy.spatial import Delaunay, ConvexHull, convex_hull_plot_2d
import numpy as np
import matplotlib.pyplot as plt
from opt import *
#from opt2 import *

def distPoints(a,b):
    return np.sqrt((b[0]-a[0])**2+(b[1]-a[1])**2)

def wToMesh(field,mesh):
    weight = np.zeros(len(mesh))
    for idx,x in enumerate(mesh):
        if (x[0],x[1]) in field:
            weight[idx] = field[(x[0],x[1])]*1000000
        else:
            weight[idx] = 0.00001 
    return  weight

def makeRectangle(a,start,size):
    rectangle = []
    pA = [start, start]
    verts = np.array([
                    [start,start],
                    [start, a-start],
                    [a-start, a-start],
                    [a-start, start]])
    closest= a-a/100
    for i in range(size):
        x = closest * np.random.rand()
        y = closest * np.random.rand()
        rectangle.append([x,y])

    return np.vstack((verts,np.asarray(rectangle))),verts

#mesh,geo_points = makeRectangle(1,0,150)
#with open('thesisMesh.npy','wb') as f:
#    np.save(f,mesh,allow_pickle=False)
start = 0.0
a = 1.0
geo_points = np.array([
                [start,start],
                [start, a-start],
                [a-start, a-start],
                [a-start, start]])
hull = ConvexHull(geo_points)
k = geo_points[hull.simplices]
n_bc = 100 
p_ar = np.column_stack((np.linspace(k[0][0][0],k[0][1][0],n_bc),np.linspace(k[0][0][1],k[0][1][1],n_bc)))
for i in range(1,len(k)):
    p_ar = np.append(p_ar,np.column_stack((np.linspace(k[i][0][0],k[i][1][0],n_bc),np.linspace(k[i][0][1],k[i][1][1],n_bc))),axis = 0)

data = np.load('v_sizing.npy',allow_pickle = False)
print(data)
mesh = np.load('thesisMesh_isotropic.npy',allow_pickle = False)
sized = np.unique(np.vstack((data[:,0:2],mesh)),axis =0)

#sized = mesh
#geo_points = np.vstack((data[:,0:2],geo_points))

field = {}
a = data[:,0:2]
b = data[:,2]
for a,b in zip(a,b):
    field[(a[0],a[1])] = b

weights = wToMesh(field,sized)
tri = Delaunay(sized, qhull_options = "QJ Pp")

for i in range(10):
    sized = updateVerts(sized,tri.simplices,p_ar,geo_points,weights)
    #sized = updateVerts(sized,tri.simplices,p_ar,geo_points,False)
    tri = Delaunay(sized, qhull_options = "QJ Pp")
    if i in [0,1,2,3,4,5,10,20,30,40,49,99,100]:
        plt.figure()
        plt.triplot(sized[:,0],sized[:,1],tri.simplices)
        plt.xlabel("x")
        plt.ylabel("y")
        plt.savefig("Iter_sizing{}.png".format(i))
        plt.close()
with open('toGMSH.npy','wb') as f:
    np.save(f,sized, allow_pickle = False)
plt.triplot(sized[:,0], sized[:,1],tri.simplices)
plt.show()