import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

data = pd.read_csv('finalstep0.csv',sep=',')
velocity = data[['Points:0','Points:1']]
velocity['Magnitude'] = np.sqrt(data['U:0']**2+data['U:1']**2)
sizingF = velocity.to_numpy()
bigboynumbers = sizingF[sizingF[:,2]>=0.25]
print(bigboynumbers.shape)
with open('v_sizing.npy','wb') as f:
    np.save(f,bigboynumbers, allow_pickle = False)
plt.scatter(bigboynumbers[:,0],bigboynumbers[:,1])
plt.show()
