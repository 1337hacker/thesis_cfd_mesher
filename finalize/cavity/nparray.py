from scipy.spatial import Delaunay, ConvexHull, convex_hull_plot_2d
import numpy as np
import matplotlib.pyplot as plt
from opt import *
#from opt2 import *

def distPoints(a,b):
    return np.sqrt((b[0]-a[0])**2+(b[1]-a[1])**2)

def dataToVertex(sim,mesh):
    weight = np.zeros(len(mesh))

    for idx,x in enumerate(mesh):
        closest = 1000
        id_closest = 0
        for idy,y in enumerate(sim):
            dist = distPoints([y[1],y[2]],x)
            if dist < closest:
                closest = dist
                id_closest = idy
        #if same vertex is the closest to multiple bc points, then 1 weighted sum
        weight[idx]=sim[id_closest][0] 
    return  weight

#mesh = np.delete(np.load("gmsh.npy",allow_pickle = False),2,1)

#TODO: random pontokat generálni aztán ezekre lefuttatni a hálózót  
def makeRectangle(a,start,size):
    rectangle = []
    pA = [start, start]
    verts = np.array([
                    [start,start],
                    [start, a-start],
                    [a-start, a-start],
                    [a-start, start]])
    closest= a-a/100
    for i in range(size):
        x = closest * np.random.rand()
        y = closest * np.random.rand()
        rectangle.append([x,y])

    return np.vstack((verts,np.asarray(rectangle))),verts

mesh,geo_points = makeRectangle(1,0,100)

tri = Delaunay(mesh, qhull_options = "QJ Pp")
hull = ConvexHull(geo_points)
k = geo_points[hull.simplices]
n_bc = 20 
p_ar = np.column_stack((np.linspace(k[0][0][0],k[0][1][0],n_bc),np.linspace(k[0][0][1],k[0][1][1],n_bc)))
for i in range(1,len(k)):
    p_ar = np.append(p_ar,np.column_stack((np.linspace(k[i][0][0],k[i][1][0],n_bc),np.linspace(k[i][0][1],k[i][1][1],n_bc))),axis = 0)

#ceTODO: Test how to scale the weights
data = np.genfromtxt('presidual.csv',delimiter = ",", skip_header=1)
data2D = np.delete(data[data[:,3]==0],3,1)
#sizing function = distance from biggest residual
#maxP = data2D[data2D[:,0]>data2D[:,0].max()*0.9]
#orig = maxP.flatten()[1:]
#print(orig)
#new_Qs = np.vstack((p_ar,maxP))
#print(new_Qs)
w = np.abs(dataToVertex(data2D,mesh))
wmin = w.min()
wmax = w.max()
w_scaled = (w/wmin)
#print(w_scaled.min(),w_scaled.max())
#top10 = w_scaled.max()*0.9
#for i in range(len(w_scaled)):
#    if i<top10:
#        w_scaled[i] = 0.001        

for i in range(100):
    mesh = updateVerts(mesh,tri.simplices,p_ar,geo_points,w_scaled)
    #mesh = updateVerts(mesh,tri.simplices,p_ar,geo_points,orig,True)
    tri = Delaunay(mesh, qhull_options="QJ Pp")
plt.triplot(mesh[:,0], mesh[:,1],tri.simplices)
plt.show()
